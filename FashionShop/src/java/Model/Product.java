/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author ASUS
 */
public class Product {

    private int productId;
    private String productnName;
    private int price;
    private String img;
    private int season;
    private String descripTion;
    private String gender;
    private int categoryId;

    public Product() {
    }

    public Product(String productnName, int price, String img, int season, String descripTion, String gender, int categoryId) {
        this.productnName = productnName;
        this.price = price;
        this.img = img;
        this.season = season;
        this.descripTion = descripTion;
        this.gender = gender;
        this.categoryId = categoryId;
    }

    public Product(int productId, String productnName, int price, String img, int season, String descripTion, String gender, int categoryId) {
        this.productId = productId;
        this.productnName = productnName;
        this.price = price;
        this.img = img;
        this.season = season;
        this.descripTion = descripTion;
        this.gender = gender;
        this.categoryId = categoryId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductnName() {
        return productnName;
    }

    public void setProductnName(String productnName) {
        this.productnName = productnName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getSeason() {
        return season;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public String getDescripTion() {
        return descripTion;
    }

    public void setDescripTion(String descripTion) {
        this.descripTion = descripTion;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Product{" + "productId=" + productId + ", productnName=" + productnName + ", price=" + price + ", img=" + img + ", season=" + season + ", descripTion=" + descripTion + ", gender=" + gender + ", categoryId=" + categoryId + '}';
    }

   

}
