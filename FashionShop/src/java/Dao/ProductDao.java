/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import Context.*;
import Model.*;
import Util.SqlQuery;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class ProductDao extends MyDao implements SqlQuery {

    public List<Product> getNewProdcuts() {
        List<Product> list = new ArrayList<>();

        try {
            ps = con.prepareStatement(SqlQuery.product.getNewProducts);
            rs = ps.executeQuery();
            Product product = null;
            int count = 0;
            while (rs.next() && count < 4) {
                product = new Product(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getInt(8));
                list.add(product);
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) {
        ProductDao a = new ProductDao();
        List<Product> list = a.getNewProdcuts();

        for (Product string : list) {
            System.out.println(string);
        }
    }

}
