/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import Model.*;
import Util.*;
import Context.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class CategoryDao extends MyDao implements SqlQuery {

//    public List<String> getAllCategoryName() {
//        List<String> list = new ArrayList<>();
//
//        try {
//            ps = con.prepareStatement(SqlQuery.category.getAllCategoryName);
//            rs = ps.executeQuery();
//            String categoryName;
//            while (rs.next()) {
//                categoryName = rs.getString(1);
//                list.add(categoryName);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return list;
//    }
    public List<Category> getAllCategoryName() {
        List<Category> list = new ArrayList<>();

        try {
            ps = con.prepareStatement(SqlQuery.category.getAllCategoryName);
            rs = ps.executeQuery();
            Category category = null;
            while (rs.next()) {
                category = new Category(rs.getInt(1), rs.getString(2));
                list.add(category);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) {
        CategoryDao a = new CategoryDao();
        List<Category> list = a.getAllCategoryName();

        for (Category string : list) {
            System.out.println(string);
        }

    }

}
