/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Util;

/**
 *
 * @author ASUS
 */
public interface SqlQuery {

    interface account {

        public static final String login = "SELECT * FROM [ACCOUNT] WHERE UserName = ? AND PassWord = ?";

        public static final String signUp = "INSERT INTO [ACCOUNT] VALUES(?,?,?,?,?)";
    }

    interface category {

//        public static final String getAllCategoryName = "SELECT [CategoryName] FROM [CATEGORIES]";
        public static final String getAllCategoryName = "SELECT * FROM [CATEGORIES]";

    }

    interface product {

        public static final String getNewProducts = "SELECT TOP(4) * FROM PRODUCTS ORDER BY CategoryID DESC";
    }

}
