﻿USE MASTER

CREATE DATABASE FashionShop3
USE FashionShop3


CREATE TABLE [CATEGORIES](
[CategoryID] INT IDENTITY (1,1) PRIMARY KEY,

[CategoryName] NVARCHAR(50) NOT NULL
)

---------------------------------------------

CREATE TABLE [PRODUCTS](
[ProductID] INT IDENTITY (1,1) PRIMARY KEY,

[ProductName] NVARCHAR(50) NOT NULL, 

[Price] MONEY CHECK (Price >0 ),

[Image] NVARCHAR(500) NOT NULL,

-- 1 for summer, 2 for winter | Shoes dosen't need season
[Season] INT CHECK( [Season] BETWEEN 1 AND 2 ),

[Description] NVARCHAR(500) NOT NULL,

--male | female
[Gender] NVARCHAR(10) NOT NULL,

[CategoryID] INT FOREIGN KEY REFERENCES [CATEGORIES](CategoryID)
)


-------------------------------------------------------------

CREATE TABLE [AGENT] (
-- HANOI DANANG SAIGON
[AgentID] INT PRIMARY KEY,

[AgentName] NVARCHAR(10)
)

/*
CREATE TABLE [AGENT-PRODUCTS](
[AgentID] INT FOREIGN KEY REFERENCES [AGENT]([AgentID]),
[ProductID] INT FOREIGN KEY REFERENCES [PRODUCTS](ProductID),
 PRIMARY KEY ([AgentID],[ProductID]) 
)
*/

CREATE TABLE [SIZE] (
[AgentID] INT FOREIGN KEY REFERENCES [AGENT]([AgentID]),
[ProductID] INT FOREIGN KEY REFERENCES [PRODUCTS](ProductID),
[Size] NVARCHAR(20) NOT NULL,
[Quantity] INT CHECK ([QUANTITY] > 0)
)

----------------------------------------------------------------

-- TABLE TO Login
CREATE TABLE [ACCOUNT] (
[UserID] INT IDENTITY (1,1) PRIMARY KEY,

[UserName] NVARCHAR(50) NOT NULL,

[PassWord] NVARCHAR(50) NOT NULL,

[PhoneNumber] INT NOT NULL,

-- 1 for Admin, 2 for HANOI employees, 3 for DANANG employees, 4 for SAIGON emloyees, 5 for Customers, anonym is null for orders
[Role] INT,

[Email] NVARCHAR(50) NOT NULL,

UNIQUE ([UserName], [EMAIL])
)

-----------------------------------------------------------------

-- TABLE TO STORES INFO OF CUSTOMERS

-------------------------------------------------------

CREATE TABLE [CART] (
[CartID] int primary key,

[UserID] INT FOREIGN KEY REFERENCES [ACCOUNT](UserID),

[ProductID] INT FOREIGN KEY REFERENCES [PRODUCTS](ProductID),

[Quantity] INT,

[Size] NVARCHAR(20) NOT NULL
)

-- TABLE TO Stores Infor of Customer'Orders

CREATE TABLE [ORDERS] (
[OrderID] INT IDENTITY (1,1) PRIMARY KEY,

[CartID] INT FOREIGN KEY REFERENCES [CART](CartID),

[Quantity] INT NOT NULL CHECK( [Quantity] > 0 ),

[Payment] MONEY NOT NULL CHECK ( [Payment] > 0 ),

[Address] NVARCHAR(100) NOT NULL,

[Region] NVARCHAR(20) NOT NULL, 

[Date] DATE,

[Status] NVARCHAR(50) not null,
)

----------------------------------------------------------------


CREATE TABLE [EULA] (
	[ID]			INT IDENTITY (1, 1),
	[EULA]			NTEXT NOT NULL,
	PRIMARY KEY		([ID]),
)



----------------------------------------------------------------- INSERT --------------------------------------------


-------------------



INSERT INTO [CATEGORIES] VALUES ( 'ÁO KHOÁC')
INSERT INTO [CATEGORIES] VALUES ( 'ÁO PHÔNG')
INSERT INTO [CATEGORIES] VALUES ( 'ÁO HOODIE')
INSERT INTO [CATEGORIES] VALUES ( 'QUẦN JEAN')
INSERT INTO [CATEGORIES] VALUES ( 'QUẦN NỈ')
INSERT INTO [CATEGORIES] VALUES ( 'GIÀY')

SELECT * FROM [CATEGORIES]

INSERT INTO [PRODUCTS] VALUES ( 
'ÁO KHOÁC GIÓ UN OVS',
300, 
'https://cdn2.boo.vn/media/catalog/product/cache/a3c240e3377445b2b4921e41d3348417/1/_/1.2.05.3.02.002.222.23-10700011-bst-1_6.jpg',
1,
'Áo khoác nam đep',
'male',
1
)

INSERT INTO [PRODUCTS] VALUES ( 
'ÁO KHOÁC NỈ NAM',
250, 
'https://cdn2.boo.vn/media/catalog/product/cache/a3c240e3377445b2b4921e41d3348417/1/_/1.2.05.2.02.002.220.01-10200011-bst-1.jpg',
2,
'Áo khoác nỉ cho nam đep',
'male',
3
)	

INSERT INTO [PRODUCTS] VALUES ( 
'ÁO PHÔNG NỮ CÁ TINH',
200, 
'https://cdn2.boo.vn/media/catalog/product/cache/a3c240e3377445b2b4921e41d3348417/1/_/1.2.02.3.18.003.122.23-10600017-bst-1.jpg',
1,
'Áo phông nữ siêu xinh',
'female',
2
)	


INSERT INTO [PRODUCTS] VALUES ( 
'QUẦN JEAN NAM ĐẸP SANG',
250, 
'https://cdn2.boo.vn/media/catalog/product/cache/a3c240e3377445b2b4921e41d3348417/1/_/1.2.21.2.02.001.122.23-60600015-bst-1.jpg',
1,
'Quần jean nam nhìn khá bụi bặm',
'male',
4
)	


INSERT INTO [PRODUCTS] VALUES ( 
'QUẦN NỈ NỮ ĐANG IU',
300, 
'https://cdn2.boo.vn/media/catalog/product/cache/a3c240e3377445b2b4921e41d3348417/1/_/1.2.11.3.02.007.222.23-11200011-bst-1.jpg',
1,
'Quần nỉ cho các bạn nữ xinh gái',
'female',
5
)	



INSERT INTO [PRODUCTS] VALUES ( 
'GIÀY NỮ CỰC XINH',
400, 
'https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/ddf629be-8bfd-457a-868c-1e579a637f7f/pegasus-turbo-next-nature-road-running-shoes-f3Q3zZ.png',
NULL,
'Giày nữ siêu cá tính',
'female',
6
)

SELECT * FROM [PRODUCTS]


INSERT INTO [AGENT] VALUES(
1, 'HANOI'
)

INSERT INTO  [AGENT] VALUES (
2, 'DANANG'
)

INSERT INTO  [AGENT] VALUES (
3, 'SAIGON'
)

SELECT * FROM[AGENT]


          ---   PRODUCT, AGENT, SIZE, QUANTITY
INSERT INTO [SIZE] VALUES(1,1, 'S', 20)
INSERT INTO [SIZE] VALUES(1,1, 'M', 20)
INSERT INTO [SIZE] VALUES(1,1, 'L', 20)

INSERT INTO [SIZE] VALUES(2,1, 'S', 15)
INSERT INTO [SIZE] VALUES(2,3, 'S', 26)
INSERT INTO [SIZE] VALUES(2,5, 'S', 10)


INSERT INTO [SIZE] VALUES(3,4, 'M', 21)
INSERT INTO [SIZE] VALUES(3,2, 'L', 10)
INSERT INTO [SIZE] VALUES(3,6, '42', 20)

SELECT * FROM [SIZE]


INSERT INTO [ACCOUNT] VALUES(
'ADMIN', 'NguyenThienThang2002', 0123456789, 1, 'nguyenthienthang@gmail.com'
)

INSERT INTO [ACCOUNT] VALUES(
'TRONG', 'nguyenhuutrong2002', 0123456781, 2, 'nguyenhuutrong@gmail.com'
)

INSERT INTO [ACCOUNT] VALUES(
'KHOA', 'nguyendangkhoa2002', 0123456782, 3, 'nguyendangkhoa@gmail.com'
)

INSERT INTO [ACCOUNT] VALUES(
'HUNG', 'hoangphihung2002', 0123456783, 4, 'hoangphihung@gmail.com'
)

INSERT INTO [ACCOUNT] VALUES(	
'QUANG', 'quangnguyen2002', 0123456784, 5, 'khoanguyen@gmail.com'
)

DELETE FROM [ACCOUNT] WHERE ROLE = 5

SELECT * FROM [ACCOUNT]


INSERT INTO [CART]  VALUES (
1, 2, 1, 3, 'S'
)

SELECT  * FROM [CART]


  ------ In process  ->   Delivered   ->   Closed
INSERT INTO [ORDERS] VALUES (
1,3, 900, ' Mai Son - Son La', 'NORTH', '1-12-2023',   'In Process' 
)


-------------

SELECT * FROM [ORDERS]


SELECT * FROM [ACCOUNT] WHERE UserName = 'admin' AND PassWord = 'NguyenThienThang2002'

Select * from [PRODUCTS]

SELECT [CategoryName] FROM [CATEGORIES]

INSERT INTO [CATEGORIES] ([CategoryName]) VALUES ( 
 'QUẦN NỈ XỊN HƠN NỮA' 
)
DELETE FROM CATEGORIES WHERE CategoryID = 6

SELECT * FROM PRODUCTS


SELECT TOP(4) * FROM PRODUCTS ORDER BY CategoryID DESC
















